
FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server

## software needed

ARG DEBIAN_FRONTEND="noninteractive"

RUN                                             \
     apt-get update --yes                       \
  && apt-get install -y --no-install-recommends \
          nodejs                                \
          npm                                   \
  && apt-get clean                              \
  && rm -rf /var/lib/apt/lists/*

